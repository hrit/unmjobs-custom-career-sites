var Page = {
  get name() {
    var path = window.location.pathname;
    var hash = window.location.hash;
    if ( (path.includes('home') && hash.includes('requisition')) || path.includes('requisition') )  {
      return 'requisition';
    }
    if (path.includes('home') && !hash.includes('requisition')) {
      return 'home';
    }
    if (path.includes('application')){
      return 'application';
    }
    if (path.includes('createprofile')) {
      return 'createprofile';
    }
    return 'default';
  },
  get jobTitle() {
    var jt = document.getElementById('job-title');
    if (typeof jt == 'undefined' || jt == null) {
     return '';
    }
    return jt.innerHTML;
  },
  get reqId() {
    if (typeof csod == 'undefined'
        || typeof csod.viewModel.reqId == 'undefined'
        || csod.viewModel.reqId == null)
    {
      return '';
    }
    return csod.viewModel.reqId;
  },
  get windowTitle(){
    switch(this.name){
      case 'home':
        document.title = 'Home - UNM Jobs Career Site :: The University of New Mexico';
        break;
      case 'requisition':
        document.title = 'Requisition: ' + this.reqId + ' - ' + this.jobTitle + ' - UNM Jobs Career Site :: The University of New Mexico';
        break;
      case 'application':
        document.title = 'Requisition: ' + this.reqId  + ' Application - UNM Jobs Career Site :: The University of New Mexico';
        break;
      default:
        document.title = 'UNM Jobs Career Site :: The University of New Mexico';
    }
    return document.title;
  }
};


// Create Mutation Observer to determine when apply now buttons appear
var observeDOM = (function(){
  var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    return function( obj, callback ){
      if( !obj || !obj.nodeType === 1 ) return; // validation

      if( MutationObserver ){
        // define a new observer
        var obs = new MutationObserver(function(mutations, observer){
            callback(mutations);
        })
        // have the observer observe obs for changes in children
        obs.observe( obj, { childList:true, subtree:true });
      }
    }
})();

// Observe the <div id="cs-root"> element:
var elementToObserve = document.getElementById('cs-root');
var topButtonFixed = false;
var bottomButtonFixed = false;
var applyNowButtonText = 'Apply Now';
var eventListenerApplyButtonExists = false;
var eventListenerApplyButtonBottomExists = false;
var finePrintExists = false;
var jobAdExists = 0;
var jobAdReqId = Page.reqId; //do this outside the observer, duh
var stylesStripped = false;
observeDOM( elementToObserve, function(m){

  Page.windowTitle; //Set the page title

  //Check for the existence of both apply now buttons
  var applyNowButton = document.querySelector('[data-tag="applyNowButton"]');
  var applyNowButtonBottom = document.querySelector('[data-tag="applyNowBtn"]');
  var signInLink = document.querySelector('[data-tag="SignInLink"]');

  if (applyNowButton && applyNowButton.innerHTML != applyNowButtonText){
    fixApplyNowButton(applyNowButton);
    addModalAttributes(applyNowButton);
    if (!eventListenerApplyButtonExists) {
      addEventListenerApplyButton(applyNowButton);
    }
  }

  if (applyNowButtonBottom && applyNowButtonBottom.innerHTML != applyNowButtonText){
    fixApplyNowButton(applyNowButtonBottom);
    addModalAttributes(applyNowButtonBottom);
    if (!eventListenerApplyButtonBottomExists) {
      addEventListenerApplyButton(applyNowButtonBottom);
    }
  }

  if (signInLink != null && signInLink.innerHTML == 'Sign In') {
    addModalAttributes(signInLink);
    addEventListenerApplyButton(signInLink);
  }

  if ( document.getElementById('fine-print') ){
     finePrintExists = true;
     addCss();
     addJobTargetTracker();
  }

  //strip styles
    if (jobAdReqId != Page.reqId){
      stylesStripped = false;
    }
   stripStyles(document.querySelectorAll('td'));


});

//Strip Styles
function stripStyles(htmlNode){
    if (htmlNode.length > 0  && stylesStripped == false) {
    for (var i = 0; i <= htmlNode.length - 1; i++) {
       htmlNode[i].innerHTML = htmlNode[i].innerHTML.replace(/style=/gi,"remstyle=");
       htmlNode[i].innerHTML = htmlNode[i].innerHTML.replace(/u>/gi,"b>");
       htmlNode[i].innerHTML = htmlNode[i].innerHTML.replace(/<font-family=/gi,"<remfont");
       htmlNode[i].innerHTML = htmlNode[i].innerHTML.replace(/size=/gi,"remsize=");
       htmlNode[i].innerHTML = htmlNode[i].innerHTML.replace(/<hr>/gi,"<remhr>");
       htmlNode[i].innerHTML = htmlNode[i].innerHTML.replace(/<HR>/gi,"<remhr>");
       htmlNode[i].innerHTML = htmlNode[i].innerHTML.replace(/<hr \/>/gi,"<remhr>");
       htmlNode[i].innerHTML = htmlNode[i].innerHTML.replace(/<HR \/>/gi,"<remhr>");
    }
    jobAdReqId = Page.reqId;
    stylesStripped = true;
  }
}

//Add data attributes to the apply button so it can communicate with the modal html
function addModalAttributes(element) {
  element.setAttribute('data-target','modal');
  element.setAttribute('data-toggle','modal');
}

// Close modal window with 'data-dismiss' attribute or when the backdrop is clicked
function closeModal() {
  var modal = document.getElementById('modal');
  modal.classList.remove('open');
}

//Removes the inner span element in the button, make it possible to click the text or button
function fixApplyNowButton(element) {
  element.innerHTML = applyNowButtonText;
}

function openModal(target) {
  var m_ID = target.getAttribute('data-target');
  document.getElementById(m_ID).classList.add('open');
}

//add the event handlers/listeners to deal with the modal dialog box
function addEventListenerApplyButton(element){
  element.addEventListener("click", function(e) {
    e = e || window.event;
    var target = e.target || e.srcElement;

    if (target.hasAttribute('data-toggle') && target.getAttribute('data-toggle') == 'modal') {

        if (target.hasAttribute('data-target')) {
          //handle clicks
          window.onclick = function(e) {
            e = e || window.event;
             //if click outside modal then close the window
            if (e.target.id == 'modal') {
              closeModal(target);
            }

            //if close button is clicked
           if (e.target.id == 'modal-close'){
              closeModal();
              //stop default behavior, prevent loading of TMS page after initial click
              e.preventDefault();
              e.stopPropagation();
              e.stopImmediatePropagation();
            }

          }
        //if the escape key is pressed, close the modal
          document.onkeydown = function(e) {
            e = e || window.event;
            var isEscape = false;
            if("key" in e) {
              isEscape = (e.key === "Escape" || e.key === "Esc");
            }
            else {
              isEscape = (e.keyCode === 27);
            }
            if (isEscape) {
              closeModal(target);
            }
          }

          //open the modal if there are no clicks or escapes
          openModal(target);
        }

    }

    //stop default behavior, prevent loading of TMS page after initial click
    e.preventDefault();
    e.stopPropagation();
    e.stopImmediatePropagation();

    eventListenerApplyButtonExists = true;
    eventListenerApplyButtonBottomExists = true;
   }, true)

}

//When the modal continue button is clicked redirect to appropriate page
  var modalContinueBtn = document.getElementById('modal-continue-btn');
  if (typeof modalContinueBtn == 'undefined' || modalContinueBtn == null) {
    console.info('no modal present');
  }
  else {
    modalContinueBtn.addEventListener('click', function(e) {
    //Modal Routing starts here
    Page.loginType = document.querySelector("[name='applicant-type']:checked").dataset.login;
    Page.adfsidp = 'https://loboauth.unm.edu/adfs/ls/idpinitiatedsignon.aspx?RelayState=RPID';
    Page.rpid = encodeURIComponent('=https://'+window.location.host+'&RelayState=');
    if (Page.name == 'home') { // when the user click's Sign In
         Page.internalJobSearchPage = "/ui/internal-career-site/app/search-jobs";
         Page.relayStateURL = encodeURIComponent(encodeURIComponent('https://'+window.location.host+Page.internalJobSearchPage)); //doubly encoded
         Page.internalDestination = Page.adfsidp + Page.rpid + Page.relayStateURL;
         Page.externalReturnURL = encodeURIComponent('~'+csod.viewModel.searchUrl+'&lang=en-US');
         Page.externalDestination = 'https://'+ window.location.host + '/ats/careersite/login.aspx?c=unm&site=18&returnurl=' + Page.externalReturnURL;
         Page.searchParams = '';
         if (Page.loginType == 'sso') {
          if (document.querySelector("[name='applicant-type']:checked").value == 'unm-student') {
             Page.searchParams = 'cf255=1917'; //selects the Student filter on the internal site
          }
          const urlParams = new URLSearchParams(window.location.search);
          var searchQuery = urlParams.get('sq');
          if (searchQuery == null) {
            Page.searchParams = '?'+Page.searchParams;
          } else
          {
            if (Page.searchParams == '') {
              Page.searchParams = '?q='+searchQuery;
            }
            else {
              Page.searchParams = '?q='+searchQuery + '&' + Page.searchParams;
            }
          }
          window.location.href = Page.internalDestination+encodeURIComponent(encodeURIComponent(Page.searchParams));
         }

        if (Page.loginType == 'csod') {
          window.location.href = Page.externalDestination;
         }
    }

    if (Page.name == 'requisition') { // when the user click's Apply Now
      if (Page.loginType == 'sso') {
        Page.jobRequistionPage = '/ui/application-workflow/application/'+csod.viewModel.reqId;
        Page.relayStateURL = encodeURIComponent(encodeURIComponent('https://'+window.location.host+Page.jobRequistionPage));
        Page.internalDestination = Page.adfsidp + Page.rpid + Page.relayStateURL;
        window.location.href = Page.internalDestination;
      }
      if (Page.loginType == 'csod') {
        Page.externalDestination = 'https://'+ window.location.host +'/ux/ats/careersite/'+ csod.viewModel.careerSiteId +'/requisition/'+ csod.viewModel.reqId +'/application' + csod.viewModel.nonCustomFieldParams;
        window.location.href = Page.externalDestination;
      }
    }

    var csodJobApplication = 'https://'+ window.location.host +'/ux/ats/careersite/'+ csod.viewModel.careerSiteId +'/requisition/'+ csod.viewModel.reqId +'/application' + csod.viewModel.nonCustomFieldParams;

      e.preventDefault();
  }, true);
}
//Add CSS to various pages or elements
function addCss(){

  //make the job ad the only printable part of the page
  var e = document.querySelector(".p-view-jobdetailsad");
  if (e != null) {
    e.className="p-view-jobdetailsad printable";
  }

  //Show the fine print on the job ad and search page
  e = document.querySelector("#fine-print");
  if (e != null) {
    if (Page.name == 'requisition'  || Page.name == 'home') {
      e.className="show";
    } else {
      e.className="hide";
    }
  }

  //Add an opaque background to the job ad
  e = document.querySelector('[data-tag="ReqTitle"]');
  if ( e != null ){
    e.parentElement.parentElement.parentElement.parentElement.className="p-panel p-p-v-xl reqTitleBackground";
  }

 //Hide Staff Job Family if Type of Position not Staff
  e1 = document.querySelector('[aria-label="Type of Position"]');
  var e2 = document.querySelector('[title="Staff Job Family"]');
  if (e1 != null && e1.options != undefined && e2 != null) {
     if (!e1.options[e1.selectedIndex].text.includes('Staff')) {
      if (e2 != null && e2.title == 'Staff Job Family'){
        e2.parentElement.parentElement.className="p-panel p-p-b-md p-bw-b-xs p-bc-b-grey70 p-bs-b-solid display-none";
      }
     } else {
      e2.parentElement.parentElement.className="p-panel p-p-b-md p-bw-b-xs p-bc-b-grey70 p-bs-b-solid";
     }
  }

  //Hide location search
  e = document.querySelector(".location-search");
  if(e != null) {
    e.parentElement.className = 'hide';
  }
  e = document.querySelector(".keyword-search");
  if (e != null) {
    e.parentElement.className = 'p-gridcol col-10-device-none';
  }
}
//Add a favicon
(function() {
    var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    link.type = 'image/x-icon';
    link.rel = 'shortcut icon';
    link.href = 'https://hr.unm.edu/themes/hr-unm/assets/img/favicon.ico';
    document.getElementsByTagName('head')[0].appendChild(link);
})();


//Add UNM Branded Fonts
(function() {
  var link = document.createElement('link');
  link.type = "text/css";
  link.rel = "stylesheet";
  link.href = "https://cloud.typography.com/7254094/6839152/css/fonts.css";

  var g1 = document.createElement('div');
  g1.id = 'googletag1';
  g1.className = 'hide';
  g1.innerHTML = 'dataLayer = [{\'site-analytics\': \'UA-12990591-5\'}];';
  document.getElementsByTagName('body')[0].appendChild(g1);

  var g2 = document.createElement('div');
  g2.id = 'googletag2';
  g2.className = 'hide';
  g2.innerHTML = '<iframe src="//www.googletagmanager.com/ns.html?id=GTM-WQT2MB" height="0" width="0" style="display:none;visibility:hidden"></iframe>';
  document.getElementsByTagName('body')[0].appendChild(g2);

  var g3 = document.createElement('div');
  g3.id = 'googletag3';
  g3.className = 'hide';
  g3.innerHTML = '(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\'//www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);})(window,document,\'script\',\'dataLayer\',\'GTM-WQT2MB\');';
  document.getElementsByTagName('body')[0].appendChild(g3);

})();

// Adds the Job Target pixel image.  We could probably send them an xhr request too.
function addJobTargetTracker(){
  var e = document.querySelector('.p-view-applicationworkflowthankyou');
  if (e != null){
    var jt = document.createElement('img');
    jt.id = 'JobTargetTracker';
    jt.src="https://www.click2apply.net/a/c/2xgW";
    jt.height="1";
    jt.width="1";
    jt.style="display:none";
    document.getElementsByTagName('body')[0].appendChild(jt);
  }
}

/** Font Awesome Icons, leave this alone **/
window.FontAwesomeKitConfig = {
    "asyncLoading": {
        "enabled": true
    },
    "autoA11y": {
        "enabled": true
    },
    "baseUrl": "https://kit-free.fontawesome.com",
    "detectConflictsUntil": null,
    "license": "free",
    "method": "css",
    "minify": {
        "enabled": true
    },
    "v4FontFaceShim": {
        "enabled": true
    },
    "v4shim": {
        "enabled": true
    },
    "version": "latest"
};
! function() {
    function r(e) {
        var t, n = [],
            i = document,
            o = i.documentElement.doScroll,
            r = "DOMContentLoaded",
            a = (o ? /^loaded|^c/ : /^loaded|^i|^c/).test(i.readyState);
        a || i.addEventListener(r, t = function() {
            for (i.removeEventListener(r, t), a = 1; t = n.shift();) t()
        }), a ? setTimeout(e, 0) : n.push(e)
    }! function() {
        if (!(void 0 === window.Element || "classList" in document.documentElement)) {
            var e, t, n, i = Array.prototype,
                o = i.push,
                r = i.splice,
                a = i.join;
            d.prototype = {
                add: function(e) {
                    this.contains(e) || (o.call(this, e), this.el.className = this.toString())
                },
                contains: function(e) {
                    return -1 != this.el.className.indexOf(e)
                },
                item: function(e) {
                    return this[e] || null
                },
                remove: function(e) {
                    if (this.contains(e)) {
                        for (var t = 0; t < this.length && this[t] != e; t++);
                        r.call(this, t, 1), this.el.className = this.toString()
                    }
                },
                toString: function() {
                    return a.call(this, " ")
                },
                toggle: function(e) {
                    return this.contains(e) ? this.remove(e) : this.add(e), this.contains(e)
                }
            }, window.DOMTokenList = d, e = Element.prototype, t = "classList", n = function() {
                return new d(this)
            }, Object.defineProperty ? Object.defineProperty(e, t, {
                get: n
            }) : e.__defineGetter__(t, n)
        }

        function d(e) {
            for (var t = (this.el = e).className.replace(/^\s+|\s+$/g, "").split(/\s+/), n = 0; n < t.length; n++) o.call(this, t[n])
        }
    }();

    function a(e) {
        var t, n, i, o;
        prefixesArray = e || ["fa"], prefixesSelectorString = "." + Array.prototype.join.call(e, ",."), t = document.querySelectorAll(prefixesSelectorString), Array.prototype.forEach.call(t, function(e) {
            n = e.getAttribute("title"), e.setAttribute("aria-hidden", "true"), i = !e.nextElementSibling || !e.nextElementSibling.classList.contains("sr-only"), n && i && ((o = document.createElement("span")).innerHTML = n, o.classList.add("sr-only"), e.parentNode.insertBefore(o, e.nextSibling))
        })
    }
    var d = function(e, t) {
            var n = document.createElement("link");
            n.href = e, n.media = "all", n.rel = "stylesheet", n.id = "font-awesome-5-kit-css", t && t.detectingConflicts && t.detectionIgnoreAttr && n.setAttributeNode(document.createAttribute(t.detectionIgnoreAttr)), document.getElementsByTagName("head")[0].appendChild(n)
        },
        c = function(e, t) {
            ! function(e, t) {
                var n, i = t && t.before || void 0,
                    o = t && t.media || void 0,
                    r = window.document,
                    a = r.createElement("link");
                if (t && t.detectingConflicts && t.detectionIgnoreAttr && a.setAttributeNode(document.createAttribute(t.detectionIgnoreAttr)), i) n = i;
                else {
                    var d = (r.body || r.getElementsByTagName("head")[0]).childNodes;
                    n = d[d.length - 1]
                }
                var c = r.styleSheets;
                a.rel = "stylesheet", a.href = e, a.media = "only x",
                    function e(t) {
                        if (r.body) return t();
                        setTimeout(function() {
                            e(t)
                        })
                    }(function() {
                        n.parentNode.insertBefore(a, i ? n : n.nextSibling)
                    });
                var s = function(e) {
                    for (var t = a.href, n = c.length; n--;)
                        if (c[n].href === t) return e();
                    setTimeout(function() {
                        s(e)
                    })
                };

                function l() {
                    a.addEventListener && a.removeEventListener("load", l), a.media = o || "all"
                }
                a.addEventListener && a.addEventListener("load", l), (a.onloadcssdefined = s)(l)
            }(e, t)
        },
        e = function(e, t, n) {
            var i = t && void 0 !== t.autoFetchSvg ? t.autoFetchSvg : void 0,
                o = t && void 0 !== t.async ? t.async : void 0,
                r = t && void 0 !== t.autoA11y ? t.autoA11y : void 0,
                a = document.createElement("script"),
                d = document.scripts[0];
            a.src = e, void 0 !== r && a.setAttribute("data-auto-a11y", r ? "true" : "false"), i && (a.setAttributeNode(document.createAttribute("data-auto-fetch-svg")), a.setAttribute("data-fetch-svg-from", t.fetchSvgFrom)), o && a.setAttributeNode(document.createAttribute("defer")), n && n.detectingConflicts && n.detectionIgnoreAttr && a.setAttributeNode(document.createAttribute(n.detectionIgnoreAttr)), d.parentNode.appendChild(a)
        };

    function s(e, t) {
        var n = t && t.addOn || "",
            i = t && t.baseFilename || e.license + n,
            o = t && t.minify ? ".min" : "",
            r = t && t.fileSuffix || e.method,
            a = t && t.subdir || e.method;
        return e.baseUrl + "/releases/" + ("latest" === e.version ? "latest" : "v".concat(e.version)) + "/" + a + "/" + i + o + "." + r
    }
    var t, n, i, o, l;
    try {
        if (window.FontAwesomeKitConfig) {
            var u, f = window.FontAwesomeKitConfig,
                m = {
                    detectingConflicts: f.detectConflictsUntil && new Date <= new Date(f.detectConflictsUntil),
                    detectionIgnoreAttr: "data-fa-detection-ignore",
                    detectionTimeoutAttr: "data-fa-detection-timeout",
                    detectionTimeout: null
                };
            "js" === f.method && (o = m, l = {
                async: (i = f).asyncLoading.enabled,
                autoA11y: i.autoA11y.enabled
            }, "pro" === i.license && (l.autoFetchSvg = !0, l.fetchSvgFrom = i.baseUrl + "/releases/" + ("latest" === i.version ? "latest" : "v".concat(i.version)) + "/svgs"), i.v4shim.enabled && e(s(i, {
                addOn: "-v4-shims",
                minify: i.minify.enabled
            })), e(s(i, {
                minify: i.minify.enabled
            }), l, o)), "css" === f.method && function(e, t) {
                var n, i = a.bind(a, ["fa", "fab", "fas", "far", "fal", "fad"]);
                e.autoA11y.enabled && (r(i), n = i, "undefined" != typeof MutationObserver && new MutationObserver(n).observe(document, {
                    childList: !0,
                    subtree: !0
                })), e.v4shim.enabled && (e.license, e.asyncLoading.enabled ? c(s(e, {
                    addOn: "-v4-shims",
                    minify: e.minify.enabled
                }), t) : d(s(e, {
                    addOn: "-v4-shims",
                    minify: e.minify.enabled
                }), t));
                e.v4FontFaceShim.enabled && (e.asyncLoading.enabled ? c(s(e, {
                    addOn: "-v4-font-face",
                    minify: e.minify.enabled
                }), t) : d(s(e, {
                    addOn: "-v4-font-face",
                    minify: e.minify.enabled
                }), t));
                var o = s(e, {
                    minify: e.minify.enabled
                });
                e.asyncLoading.enabled ? c(o, t) : d(o, t)
            }(f, m), m.detectingConflicts && ((u = document.currentScript.getAttribute(m.detectionTimeoutAttr)) && (m.detectionTimeout = u), document.currentScript.setAttributeNode(document.createAttribute(m.detectionIgnoreAttr)), t = f, n = m, r(function() {
                var e = document.createElement("script");
                n && n.detectionIgnoreAttr && e.setAttributeNode(document.createAttribute(n.detectionIgnoreAttr)), n && n.detectionTimeoutAttr && n.detectionTimeout && e.setAttribute(n.detectionTimeoutAttr, n.detectionTimeout), e.src = s(t, {
                    baseFilename: "conflict-detection",
                    fileSuffix: "js",
                    subdir: "js",
                    minify: t.minify.enabled
                }), e.async = !0, document.body.appendChild(e)
            }))
        }
    } catch (e) {}
}();