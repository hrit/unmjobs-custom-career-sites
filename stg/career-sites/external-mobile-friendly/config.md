Title: New - Search - MFCE Test
Allow search engines to index site: [checked]
Logo Image for Search Engines: UNMLogo.png
Header: [Simple]
  Select Header: unmtransparentwhite.png
  Header Color: BA0C2F
Footer: [Advanced]
  See footer.html
Login Page Background: umpennantsbw.jpg
Job Details Banner Image: Agave.png
Connect With Us URL: N/A
Active: [Checked]
Career Site Analytics:
  Google Tag Manager ID: [null] (included in footer JavaScript)
Add Custom Requistion Fields to Career Site:
    | Name               | Question Type | Allow Multi-Select | Include in Main Search |
    |--------------------|---------------|--------------------|------------------------|
    | Type of Position   | Dropdown      | x                  |                        |
    | Staff Job Family   | Dropdown      | x                  |                        |
    | Salary Grade       | Dropdown      | x                  |                        |
    | Campus             | Dropdown      |                    |                        |
    | Academic Location  | Dropdown      |                    |                        |
Edit Layout: 
  Banner Image: ScreenShot20191205at104104AM_54525d7b-679e-44f7-b8d4-7a7c45db681c.png
  WYSIWYG 1: [Null]
  WYSIWYG 2: [Null]