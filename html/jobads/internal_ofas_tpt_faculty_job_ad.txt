<link href="https://hr.unm.edu/themes/hr-unm/assets/css/tms-internal-jobad.css" rel="stylesheet">
<div style="padding-top: 1%">&nbsp;</div>
<h2>JOB.TITLE</h2>
<table class="table" style="max-width: 800px; font-size: 14px">
	<tbody>
		<tr><th style="width: 20%">Posting Number</th><td>REQUISITION.ID</td></tr>
		<tr><th>Employment Type</th><td>EMPLOYMENT.TYPE</td></tr>
		<tr><th>Faculty Type</th><td>CUSTOM.FACULTY.TYPE</td></tr>
		<tr><th>Hiring Department</th><td>OU.LOCATION</td></tr>
		<tr><th>Academic Location</th><td>CUSTOM.ACADEMIC.LOCATION</td></tr>
		<tr><th>Campus</th><td>CUSTOM.CAMPUS</td></tr>
		<tr><th>Benefits Eligible</th><td>CUSTOM.BENEFITS.STATEMENT</td></tr>
		<tr><th>Position Summary</th><td>JOB.DESCRIPTION</td></tr>
		<tr><th>Qualifications</th><td>JOB.MINIMUM.QUALIFICATIONS</td></tr>
		<tr><th>Application Instructions</th><td>CUSTOM.APPLICATION.INSTRUCTIONS
		                                         <br><p>Applicants who are appointed to a UNM faculty position are required to provide an official certification of successful completion of all degree requirements prior to their initial employment with UNM.</p>
		                                     </td>
	    </tr>
		<tr><th>For Best Consideration</th><td>This posting will remain active until CUSTOM.BEST.CONSIDERATION.DATE. Anyone who applies for a position under this posting will automatically be considered for openings in subsequent semesters unless they indicate in their cover letter that they are not interested in teaching in specific semesters.</td></tr>
		<tr>
			<td colspan="2">
				<br><p>The University of New Mexico is committed to hiring and retaining a diverse workforce. We are an Equal Opportunity Employer, making decisions&nbsp;without regard to race, color, religion, sex, sexual orientation, gender identity, national origin, age, veteran status, disability, or any&nbsp;other protected class.</p>
			</td>
		</tr>
	</tbody>
</table>
