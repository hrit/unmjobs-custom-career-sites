# Custom Career Site Configuration

Configure the Career Site options as follows:

| Option                             | Value                          |
| ---------------------------------- | ------------------------------ |
| Title                              | New Default - Mobile Friendly  |
| Allow search engines to index site | [Checked]                        |
| Logo Image for Search Engines      | UNMLogo.png                    |

## Header

| Option                             | Value                          |
| ---------------------------------- | ------------------------------ |
| Header                             | Advanced                       |
| Select Header                      | N/A                            |
| Header Color                       | N/A                            |
| Header WYSIWYG                     | Insert contents of header.html |

## Footer

| Option                   | Value                          |
| ------------------------ | ------------------------------ |
| Footer                   | Advanced                       |
| Footer WYSIWYG           | Insert contents of footer.html |
| Login Page Background    | door.jpg                       |
| Job Details Banner Image | Vigas.png                      |
| Connect With Us URL      | N/A                            |
| Active                   | [Checked]                      |

## Career Site Analytics

| Option                             | Value                          |
| ---------------------------------- | ------------------------------ |
| Google Tag Manager ID              | [null] (included in footer JavaScript)|

## Add Custom Requisition Fields to Career Site

| Name              | Question Type | Allow Multi-Select | Include in Main Search |
| ----------------- | ------------- | ------------------ | ---------------------- |
| Type of Position  | Dropdown      | x                  |                        |
| Staff Job Family  | Dropdown      | x                  |                        |
| Salary Grade      | Dropdown      | x                  |                        |
| Campus            | Dropdown      |                    |                        |
| Academic Location | Dropdown      |                    |                        |

## Layout

| Option                   | Value                          |
| ------------------------ | ------------------------------ |
| Edit Layout              | Banner Image: ScreenShot20191205at104104AM_54525d7b-679e-44f7-b8d4-7a7c45db681c.png |
| WYSIWYG 1                   | [null]                      |
| WYSIWYG 2                   | [null]                      |

## Footer Template

The footer WYWSIWYG should have this structure:

````html
<style>
  Contents of footer.css
</style>
<script>
  Contents of footer.js
</script>
  HTML for the actual page/footer.
````
